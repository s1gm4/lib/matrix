const Matrix = require('../lib/matrix.js');

test('matrix creation', () => {
    let a  = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    let string = "[[1, 2, 3], \n[4, 5, 6], \n[7, 8, 9]]";
    let A = Matrix.from(a);
    expect(A.toString()).toBe(string);
});

test('matrix determinant calculation', () => {
    let m = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let d = m.det();
    expect(d).toBe(0);
});

test('matrix transposition', () => {
    let m = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let t = m.T;
    let string = "[[1, 4, 7], \n[2, 5, 8], \n[3, 6, 9]]";
    expect(t.toString()).toBe(string);
});

// Test all the matrix operations
test('matrix addition', () => {
    let m1 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m2 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m3 = m1.add(m2);
    let string = "[[2, 4, 6], \n[8, 10, 12], \n[14, 16, 18]]";
    expect(m3.toString()).toBe(string);
});

test('matrix subtraction', () => {
    let m1 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m2 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m3 = m1.sub(m2);
    let string = "[[0, 0, 0], \n[0, 0, 0], \n[0, 0, 0]]";
    expect(m3.toString()).toBe(string);
});

test('matrix multiplication', () => {
    let m1 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m2 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m3 = m1.mul(m2);
    let string = "[[30, 36, 42], \n[66, 81, 96], \n[102, 126, 150]]";
    expect(m3.toString()).toBe(string);
});

test('matrix scalar multiplication', () => {
    let m1 = Matrix.from([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    let m2 = m1.scale(2);
    let string = "[[2, 4, 6], \n[8, 10, 12], \n[14, 16, 18]]";
    expect(m2.toString()).toBe(string);
});

// Test matrix inversion
test('matrix inversion', () => {
    let m1 = Matrix.from([[1, 1], [1, 0]]);
    let m2 = m1.inv();
    let string = "[[0, -1], \n[1, -1]]";
    expect(m2.toString()).toBe(string);
});